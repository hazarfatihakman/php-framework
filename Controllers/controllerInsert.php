<?php

require '../Models/DbContext.php';

$Db = new DbContext();
$ModelName = "\\Models\\".$_POST['Model'];

$Model = new $ModelName();

unset($_POST['Model']);

$Db->Insert($_POST,$Model);

header("location:".$_SERVER['HTTP_REFERER']);
