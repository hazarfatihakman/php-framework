<?php
namespace Models;

class User {
    public int $Id = 0;
    public string $firstName;
    public string $lastName;

    public function Config(){
        //return array('Id.INT','firstName.VARCHAR(100)','lastName.VARCHAR(100)');        
        return array('Id.INT','firstName.VARCHAR(100)','lastName.VARCHAR(100)');
    }
    public function Uniq(){
        //return array('PRIMARY KEY(Id)','UNIQUE ( `firstName`, `lastName`)');
        return array('PRIMARY KEY(Id)');
    }
}
