<?php
$base_url ="C:/xampp/htdocs/php-framework";
require_once $base_url."/Framework/DB.php";

include 'User.php';
include 'Todo.php';

class DbContext extends Db {
      public function __construct(){
          parent::__construct();
      }
      
      public function Start(){
          $User = new \Models\User();
          $this->DBSet($User);
          $Todo = new \Models\Todo();
          $this->DBSet($Todo);      
      }


      public function __destruct(){
          parent::__destruct();
      }


}