<?php

namespace Models;

class Todo {
    public int $Id;
    public int $userId;
    public string $todo;
    public string $date;

    public function Config(){
        return array('Id.INT','userId.INT','todo.TEXT','date.DATE');
    }
    public function Uniq(){
        return array('PRIMARY KEY(Id)');
    }
}