<?php

include "Models/DbContext.php";

$Db = new DB();
$DbContext = new DbContext();
$User = new \Models\User();
$DbContext->Start();
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td,
            th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Data Framework</title>
    </head>

    <body>
        <div>
            <form action="Controllers/controllerInsert.php" method="POST">
                <h1>Insert</h1>
                <input type="text" name="firstName" placeholder="First Name">
                <input type="text" name="lastName" placeholder="Last Name">

                <input type="text" name="Model" value="User" hidden>
                <input type="submit" placeholder="Gönder">
            </form>
            
                <h1>Update</h1>
                <table>
                    <tr>
                        <th>Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Change</th>
                    </tr>
                <?php
                $users = $Db->Get($User);  
                    if($users[0] != "Not found"){
                        foreach($users as $user) {
                            echo '
                            <form action="Controllers/controllerUpdate.php" method="POST">
                                <tr>
                                    <th><input type="text" name="Id" value="'.$user->Id.'" hidden>'.$user->Id.'</th>
                                    <th><input type="text" name="firstName" placeholder="First Name" value="'.$user->firstName.'"></th>
                                    <th><input type="text" name="lastName" placeholder="Last Name" value="'.$user->lastName.'"></th>
                                    <th><button type="submit" name="Where" value="Id">Change</button></th>
                                </tr>
                                <input type="text" name="Model" value="User" hidden>
                            </form>';
                        }
                    }
                    else {
                        echo '  <tr>
                                    <th>'.$users[0].'</th>
                                    <th>'.$users[0].'</th>
                                    <th>'.$users[0].'</th>
                                    <th>'.$users[0].'</th>
                                </tr>';
                    }
                ?>
                </table>
               
                <h1>Remove</h1>
                <table>
                    <tr>
                        <th>Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Remove</th>
                    </tr>
                <?php
                $users = $Db->Get($User);
                    if($users[0] != "Not found"){
                        foreach($users as $user) {
                            echo '
                            <form action="Controllers/controllerRemove.php" method="POST">
                                <tr>
                                    <th><input type="text" name="Value" value="'.$user->Id.'" hidden>'.$user->Id.'</th>
                                    <th>'.$user->firstName.'</th>
                                    <th>'.$user->lastName.'</th>
                                    <th><button type="submit" name="Where" value="Id">Remove</button></th>
                                </tr> 
                                
                                <input type="text" name="Model" value="User" hidden>
                            </form>';
                        }
                    }
                    else {
                        echo '  <tr>
                                    <th>'.$users[0].'</th>
                                    <th>'.$users[0].'</th>
                                    <th>'.$users[0].'</th>
                                    <th>'.$users[0].'</th>
                                </tr>';
                    }
                ?>
                </table>
                <h1>Get One User</h1>    
                <?php
                    $User->Id = 2;
                    $getone = $Db->GetOne($User);
                    if(gettype($getone) != "array"){
                        echo "First Name : ".$getone->firstName."<br>";
                        echo "Last Name : ".$getone->lastName."<br>"; 
                    }
                    else {
                        echo $getone[0];
                    }
               
                ?>

        </div>


    </body>

</html>