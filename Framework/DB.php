<?php
class DB {    
    protected $con, $host = "localhost", $user="root", $passWord="", $db="api",
              $charSet = 'utf8', $charSetconf = 'utf8mb4', $Collate = 'utf8mb4_turkish_ci';
    public function __construct(){
        $this->con = new mysqli($this->host,$this->user,$this->passWord);            
        $creatSql = 'CREATE DATABASE '.$this->db.' CHARACTER SET '.$this->charSetconf.' COLLATE '.$this->Collate;
        if (mysqli_query($this->con, $creatSql)) {
            echo "<br>Database created successfully<br>";
            $this->con = new mysqli($this->host,$this->user,$this->passWord,$this->db);
        }
        else {
            $this->con = new mysqli($this->host,$this->user,$this->passWord,$this->db);            
        }
        $this->con->set_charset($this->charSet);
    }

    public function DBSet($Model){ 
        $nameSpace = new \ReflectionClass($Model);
        $nameSpaceGet = $nameSpace->getNamespaceName()."\\";
        $table = strtolower(str_replace($nameSpaceGet,"",get_class($Model)));
        $config = $Model->Config();
        $uniq = $Model->Uniq();
        $newStr = 'CREATE TABLE '.$table.' (';
        for($i=0; $i < count($config);$i++){
            $newCon = explode('.',$config[$i]);
            if($i == 0){                
                $newStr = $newStr.$newCon[0].' '.$newCon[1].' AUTO_INCREMENT, ';
            }
            else {
                $newStr = $newStr.$newCon[0].' '.$newCon[1].',';
            }

        }
        for($i=0; $i < count($uniq); $i++){            
            if($i == count($uniq)-1){
                $newStr = $newStr.' '.$uniq[$i].')';
            }
            else {                
                $newStr = $newStr.' '.$uniq[$i].',';
            }
        }
        if($this->con->query($newStr)){
            echo "<br>Database created successfully<br>";
        }
    }

    public function Get($Model){
        $nameSpace = new \ReflectionClass($Model);  
        $nameSpaceGet = $nameSpace->getNamespaceName()."\\";
        $table = strtolower(str_replace($nameSpaceGet,"",get_class($Model)));
        $newSql = "select * from ".$table;
        $query = $this->con->query($newSql);       
        $data = array();
        
        while($row = \mysqli_fetch_assoc($query)){
            $newModel = get_class($Model);
            $newModel = new $newModel();
            $rowKey = array_keys($row);
            foreach($rowKey as $dataKey){                
                $newModel->$dataKey = $row[$dataKey];
            }
            array_push($data,$newModel);
          
        }       
        if($data == null){
            return array("Not found");
        }else {
            return $data;
        }
    }

    public function Remove($Model) {
        $nameSpace = new \ReflectionClass($Model);  
        $nameSpaceGet = $nameSpace->getNamespaceName()."\\";
        $table = strtolower(str_replace($nameSpaceGet,"",get_class($Model)));

        $where = array_keys((array)$Model)[0];

        $newSql = "delete from ".$table." where ".$where." = ".$Model->$where;
        $query = $this->con->query($newSql);      
        if(!$query) {
            echo trigger_error(mysqli_error($this->con));
        }
        //modifed multiple selected column values 
    }

    public function GetOne($Model){
        $nameSpace = new \ReflectionClass($Model);  
        $nameSpaceGet = $nameSpace->getNamespaceName()."\\";
        $table = strtolower(str_replace($nameSpaceGet,"",get_class($Model)));      
        //var_dump($Model);
        $where = array_keys((array)$Model)[0];
       
        //echo $where;
        $newSql = "select * from ".$table." where ".$where." = ".$Model->$where;        
        $query = $this->con->query($newSql);

        while($row = \mysqli_fetch_assoc($query)){            
            $rowKey = array_keys($row);
            foreach($rowKey as $dataKey){                
                $Model->$dataKey = $row[$dataKey];
            }
        } 
        //var_dump($query);
        if($query->num_rows <= 0){
            return array("Not found");
        }else {
            return $Model;
        }

    }

    public function Insert($Post,$Model){
        $nameSpace = new \ReflectionClass($Model);  
        $nameSpaceGet = $nameSpace->getNamespaceName()."\\";
        $table = strtolower(str_replace($nameSpaceGet,"",get_class($Model)));      
        foreach(array_keys($_POST) as $p){
                //echo $p."<br>";
                $Model->$p = $Post[$p];
                //echo $Model->$p."<br>";
        }

        $newSql = "insert into ".$table."(";
        $config = $Model->Config();        
        $count = count($config);
        for($i=0;$i < $count;$i++){
            $newCon = explode(".",$config[$i]);
            if($i == $count-1){
                $newSql = $newSql.$newCon[0].") ";
            }
            else{
                $newSql = $newSql.$newCon[0].",";
            }
        }
        $newSql = $newSql." Values(";
        $Model = $Model;
        $count = count((array)$Model);
        $modelKeys = array_keys((array)$Model);
        //var_dump($modelKeys);
        $i = 1;
        foreach($modelKeys as $mkey) {
            if($i == $count){
                $newSql = $newSql."'".$Model->$mkey."') ";
            }
            else {
                $newSql = $newSql."'".$Model->$mkey."',";
            }         
            $i++;
        }
        $query = $this->con->query($newSql);
        if(!$query) {
            echo trigger_error(mysqli_error($this->con));
        }

    }

    public function Update($Post,$Model){
        $nameSpace = new \ReflectionClass($Model);  
        $nameSpaceGet = $nameSpace->getNamespaceName()."\\";
        $table = strtolower(str_replace($nameSpaceGet,"",get_class($Model)));
        $where = $Post['Where'];
        unset($Post['Where']);      
        foreach(array_keys($Post) as $p){
                //echo $p."<br>";
                $Model->$p = $Post[$p];
                //echo $Model->$p."<br>";
        }

        $newSql = "update ".$table." set ";
        $keys = array_keys($Post);
        $count = count($keys);
        $i = 1;
        foreach($keys as $key) {
            if($i == $count){
                $newSql = $newSql.$key." = '".$Model->$key."' ";
            }
            else {                        
                $newSql = $newSql.$key." = '".$Model->$key."', ";
            }
            $i++;
        }
        $newSql = $newSql." where ".$where." = ".$Model->$where;
        $query = $this->con->query($newSql);
        if(!$query) {
            echo trigger_error(mysqli_error($this->con));
        }  
    }

    public function __destruct() {
        mysqli_close($this->con);
   }

}


?>